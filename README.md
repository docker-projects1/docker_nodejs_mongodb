## Docker - Node JS Container

## Versions

- Docker client: 20.10.1-ce

## Docker File

```
FROM node:10-alpine
RUN mkdir /home/node/nodejs
RUN chown -R node. /home/node
WORKDIR /home/node/nodejs
COPY package.json  /home/node/nodejs/
COPY package-lock.json  /homt/node/nodesjs/
RUN npm install
COPY . .
EXPOSE 8080

CMD [ "node", "app.js" ]

```

## Docker compose

```
version: "3"
services:
  node:
    container_name: nodejs
    build: .
    ports:
      - "80:8080"
    volumes:
      -  volume:/home/node/nodejs
    networks:
      - nodenetwork

  mongodb:
    container_name: mongodb
    image: mongo
    volumes:
      - mongo:/opt/bitnami/mongodb/bin/
    networks:
      - nodenetwork
    ports:
      - '27018:27017'

networks:
  nodenetwork:
volumes:
  volume:
  mongo:


```

