FROM node:10-alpine
RUN mkdir /home/node/nodejs
RUN chown -R node. /home/node
WORKDIR /home/node/nodejs
COPY package.json  /home/node/nodejs/
COPY package-lock.json  /homt/node/nodesjs/
RUN npm install
COPY . .
EXPOSE 8080

CMD [ "node", "app.js" ]
